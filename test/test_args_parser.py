"""Test args parser."""
from unittest import TestCase

from ugen import add_arguments, get_args, get_argument_parser


class ParserTest(TestCase):
    """Test parser class."""

    def setUp(self):
        """Prepare argument parser."""
        parser = get_argument_parser()
        self.parser = add_arguments(parser)

    def test_parse_no_args(self):
        """Test if exit with no arguments specified."""
        with self.assertRaises(SystemExit):
            get_args()

    def test_parse_option_o(self):
        """Test if parser returns its corresponding variable."""
        parser = self.parser.parse_args(['-o', 'test.txt', 'foo'])
        self.assertTrue(parser.output)

    def test_parse_option_output(self):
        """Test if parser returns its corresponding variable."""
        parser = self.parser.parse_args(['--output', 'test.txt', 'foo'])
        self.assertTrue(parser.output)

    def test_parse_option_output_has_right_value(self):
        """Test if parser returns its corresponding variable value."""
        parser = self.parser.parse_args(['-o', u'test.txt', 'foo'])
        self.assertEqual(parser.output, u'test.txt')

    def test_parse_has_one_input_file(self):
        """Test if parser returns its corresponding variable value."""
        parser = self.parser.parse_args(['-o', u'out.txt', 'input.txt'])
        self.assertEqual(parser.input_file, u'input.txt'.split())

    def test_parse_has_more_input_files(self):
        """Test if parser returns its corresponding variable value."""
        parser = self.parser.parse_args(
            [u'test.txt', u'test2.txt', '-o foo.bar'])
        self.assertEqual(parser.input_file, u'test.txt test2.txt'.split())
