"""Test generating usernames."""
from unittest import TestCase

from ugen import (create_username, dump_user, dump_users, DuplicityError,
                  EmptyUsernameError, KeywordAttributesError,
                  NotValidValuesError, NoUserIdError, populate_userslist,
                  PositionalAttributesError, User, Users, UserIdIntegerError,
                  USER_ATTRS,
                  )
from ugen import parse_line


class UserInitTest(TestCase):
    """Test User class."""

    def test_can_not_set_positional_attribute(self):
        with self.assertRaises(PositionalAttributesError):
            User("bar")

    def test_can_not_set_not_defined_kw_attribute(self):
        with self.assertRaises(KeywordAttributesError):
            User(nickname="bar")

    def test_empty_init_should_not_raise_error(self):
        User()


class UserNotValidValuesTest(TestCase):

    user = User()

    def test_should_raise_error(self):
        with self.assertRaises(NotValidValuesError):
            self.user.create_username()


class FirstNameMissingTest(UserNotValidValuesTest):

    user = User(mid_name="bar", last_name="baz")


class LastNameMissingTest(UserNotValidValuesTest):

    user = User(first_name="foo", mid_name="bar")


class MidNameEmptyTest(UserNotValidValuesTest):

    user = User(first_name="foo", mid_name="bar")


class UserUsernameTest(TestCase):

    def setUp(self):
        self.user = User(first_name="foo", last_name="baz")
        self.user.create_username()

    def test_username_is_not_none_when_first_and_last_name_set(self):
        self.assertNotEqual("", self.user.username)

    def test_first_char_of_username_is_first_char_of_first_name(self):
        self.assertEqual('f', self.user.username[0])

    def test_last_chars_of_username_are_from_last_name(self):
        self.assertEqual('baz', self.user.username[1:])

    def test_username_second_char_is_first_char_of_mid_name(self):
        self.user.mid_name = "joe"
        self.user.create_username()

        self.assertEqual('j', self.user.username[1])

    def test_username_is_lower(self):
        user = User(first_name="Foo", mid_name="Bar", last_name="Baz")
        user.create_username()

        self.assertEqual('fbbaz', user.username)


class UserUsernameLengthTest(TestCase):

    def setUp(self):
        self.user = User(user_id="123", first_name="foo", last_name="bazbazbaz")

    def test_len_of_username_w_mid_name_is_less_then_max_available(self):
        self.user.mid_name = "joe"
        self.user.create_username()

        self.assertLessEqual(
            len(self.user.username),
            self.user.USERNAME_BASE_MAX_LENGTH)

    def test_len_of_username_is_not_greater_then_max_available(self):
        self.user.create_username()

        self.assertLessEqual(
            len(self.user.username),
            self.user.USERNAME_BASE_MAX_LENGTH)

    def test_username_has_default_index_when_indexing_enabled(self):
        self.user.create_username(indexed=True)

        self.assertEqual('1', self.user.username[8:])

    def test_username_has_set_index_when_indexing_enabled_and_index_is_set(self):
        self.user.create_username(indexed=True, index=234)

        self.assertEqual('234', self.user.username[8:])

    def test_get_user_attrs_in_required_order(self):
        self.user.create_username()

        self.assertListEqual(
            ['bazbazbaz', 'foo', '123', 'fbazbazb'],
            self.user.get_values_by_attrs(
                "last_name first_name user_id username".split()
            )
        )


class UserAttributesBadCasesTest(TestCase):

    values = ['', ' ', 'a a', 'abc def', 'ad ', 'a\ts', '\n\n', '@#$']
    good_value = "barbaz"

    def test_for_raising_error_on_first_name(self):
        for value in self.values:
            user = User(first_name=value, last_name=self.good_value)
            with self.assertRaises(NotValidValuesError):
                user.create_username()

    def test_for_raising_error_on_last_name(self):
        for value in self.values:
            user = User(first_name=self.good_value, last_name=value)
            with self.assertRaises(NotValidValuesError):
                user.create_username()

    def test_for_raising_error_on_mid_name(self):
        values = self.values
        values.pop(0)
        for value in values:
            user = User(
                first_name=self.good_value,
                mid_name=value,
                last_name=self.good_value
            )
            with self.assertRaises(NotValidValuesError):
                user.create_username()


class UsernameGoodCasesTest(TestCase):

    values = ['\u0394', '\u51b2', ]
    good_value = "barbaz"

    def test_should_not_raising_error_on_first_name(self):
        for value in self.values:
            user = User(first_name=value, last_name=self.good_value)
            user.create_username()

    def test_should_not_raising_error_on_last_name(self):
        for value in self.values:
            user = User(first_name=self.good_value, last_name=value)
            user.create_username()

    def test_should_not_raising_error_on_mid_name(self):
        values = self.values.pop(0)
        for value in values:
            user = User(
                first_name=self.good_value,
                mid_name=value,
                last_name=self.good_value
            )
            user.create_username()


class UsersTest(TestCase):
    """Users class tests."""

    def setUp(self):
        self.users_list = Users()

    def test_users_list_is_empty(self):
        self.assertEqual(0, len(self.users_list))

    def test_can_add_user_to_users_list(self):
        user = User(user_id=1, first_name="foo", last_name="bar")
        user.create_username()
        self.users_list.add(user)

        self.assertEqual(1, len(self.users_list))

    def test_can_not_add_user_to_users_list_wo_user_id_set(self):
        user = User(first_name="foo", last_name="bar")
        user.create_username()

        with self.assertRaises(NoUserIdError):
            self.users_list.add(user)

    def test_can_not_add_user_to_users_list_w_user_id_set_to_no_int(self):
        user = User(user_id="foo", first_name="foo", last_name="bar")
        user.create_username()

        with self.assertRaises(UserIdIntegerError):
            self.users_list.add(user)

    def test_users_list_empty_in_another_instance(self):
        user = User(user_id=1, first_name="foo", last_name="bar")
        user.create_username()
        self.users_list.add(user)
        self.users_list = Users()

        self.assertEqual(0, len(self.users_list))

    def test_can_not_add_user_w_empty_username(self):
        user = User(user_id=1, first_name="foo", last_name="barbaz")
        with self.assertRaises(EmptyUsernameError):
            self.users_list.add(user)

    def test_username_is_not_used(self):
        user = User(user_id=1, first_name="foo", last_name="barbaz")
        user.create_username()

        self.assertFalse(
            self.users_list.is_username_used(user.username))

    def test_username_is_used(self):
        user = User(user_id=1, first_name="foo", last_name="barbaz")
        user.create_username()
        self.users_list.add(user)

        self.assertTrue(
            self.users_list.is_username_used(user.username))

    def test_usernames_are_unique_in_users_list(self):
        user = User(user_id=1, first_name="foo", last_name="barbaz")
        user.create_username()
        self.users_list.add(user)

        with self.assertRaises(DuplicityError):
            self.users_list.add(user)

    def test_users_list_is_iterable(self):
        for i in range(0, 2):
            user = User(user_id=1, 
                        first_name="{}".format(i), last_name="barbaz")
            user.create_username()
            self.users_list.add(user)

        self.assertTrue(hasattr(self.users_list, '__iter__'))

    def test_users_list_can_return_list_of_users_objects(self):
        import ugen
        for i in range(0, 3):
            user = User(user_id=1, 
                        first_name="{}".format(i), last_name="barbaz")
            user.create_username()
            self.users_list.add(user)

        for user in self.users_list:
            self.assertEqual(ugen.User, type(user))

    def test_users_list_return_correct_count_of_users_objects(self):
        for i in range(0, 3):
            user = User(user_id=1, first_name="{}".format(i),
                        last_name="barbaz")
            user.create_username()
            self.users_list.add(user)

        self.assertEqual(3, len(self.users_list))

    def test_returned_list_of_users_contain_user_objects(self):
        user = User(user_id=1, first_name="foo", last_name="barbaz")
        user.create_username()
        self.users_list.add(user)

        self.assertEqual(user, self.users_list.users.pop())

    def test_users_order(self):
        for i in range(0, 100):
            user = User(user_id=1, first_name="foo", last_name="barbaz")
            user.create_username(indexed=True, index=i)
            self.users_list.add(user)

        i = 0
        for user in self.users_list:
            self.assertEqual(user.username, "fbarbaz{}".format(i))
            i += 1


class ModuleTest(TestCase):
    """Test all."""

    def setUp(self):
        self.users = Users()
        self.values = "1234 foo ss barbarbar depp1".split()
        self.user = User(**dict(zip(USER_ATTRS, self.values)))

    def test_create_username(self):
        create_username(self.user, self.users)

        self.assertEqual('fsbarbar', self.user.username)

    def test_generating_unique_usernames_when_same_values(self):
        create_username(self.user, self.users)
        self.users.add(self.user)
        user2 = User(**dict(zip(USER_ATTRS, self.values)))
        create_username(user2, self.users)
        self.users.add(user2)

        self.assertNotEqual(self.user.username, user2.username)
        self.assertEqual('fsbarbar1', user2.username)

    def test_populating_userlist(self):
        lines = "1234:foo:mid:last:depp\n2345:bar:mid:last:depp".split("\n")
        data = [parse_line(line) for line in lines]
        populate_userslist(self.users, data)

        self.assertEqual(2, len(self.users))

    def test_dump_user(self):
        lines = "1234:foo:mid:last:depp\n2345:bar:mid:last:depp".split("\n")
        data = [parse_line(line) for line in lines]
        populate_userslist(self.users, data)

        dump = ""
        # for user in self.users:
        dump = "{}{}\n".format(dump, dump_user(self.users.users[1]))

        self.assertEqual(
            "2345:bmlast:bar:mid:last:depp\n",
            dump
        )

    def test_dump_users(self):
        lines = "1234:foo:mid:last:depp\n2345:bar:mid:last:depp".split("\n")
        data = [parse_line(line) for line in lines]
        populate_userslist(self.users, data)

        self.assertEqual(
            "1234:fmlast:foo:mid:last:depp\n2345:bmlast:bar:mid:last:depp\n",
            dump_users(self.users)
        )
