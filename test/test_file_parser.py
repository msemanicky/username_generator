"""Test file parser."""
from unittest import TestCase
from unittest.mock import patch, mock_open

from ugen import DELIMITER_CHAR
from ugen import DelimiterError, EmptyFileError, LineItemsCountError
from ugen import parse_file


class FileParserTest(TestCase):
    """Test File parser class."""

    def test_parse_empty_file(self):
        with patch('builtins.open', mock_open(read_data="")):
            with self.assertRaises(EmptyFileError):
                parse_file('foo')

    def test_parse_more_empty_lines(self):
        with patch('builtins.open', mock_open(read_data="\n\n")):
            with self.assertRaises(DelimiterError):
                parse_file('foo')

    def test_parse_file_w_line_contains_only_simple_text(self):
        test_txt = "1234"
        with patch('builtins.open', mock_open(read_data=test_txt)):
            with self.assertRaises(DelimiterError):
                parse_file('foo')

    def test_parse_file_w_line(self):
        test_txt = "1234:foo:bar:dep"
        with patch('builtins.open', mock_open(read_data=test_txt)):
            results = parse_file('foo')

        self.assertListEqual(
            [item.split(DELIMITER_CHAR) for
             item in test_txt.split("\n")],
            results
        )

    def test_parse_file_w_more_lines(self):
        test_txt = "1234:foo:bar:dep\n2345:bbb:ddd:dep\n11:123:22:222:22"
        with patch('builtins.open', mock_open(read_data=test_txt)):
            results = parse_file('foo')

        self.assertListEqual(
            [item.split(DELIMITER_CHAR) for
             item in test_txt.split("\n")],
            results
        )

    def test_parse_file_w_more_lines_and_extra_empty_line(self):
        test_txt = "1234:foo:bar:dep\n2345:bbb:ddd:dep\n11:123:22:222:22\n"
        with patch('builtins.open', mock_open(read_data=test_txt)):
            results = parse_file('foo')

        items = [item.split(DELIMITER_CHAR) for
                 item in test_txt.split("\n")]
        items.pop()

        self.assertListEqual(items, results)

    def test_parse_file_w_more_lines_and_specials(self):
        test_txt = "1234:foo  :ba\t r:dep\n23\\\45:bbb::dep\n1:123:22:222:22\n"
        with patch('builtins.open', mock_open(read_data=test_txt)):
            results = parse_file('foo')

        items = [item.split(DELIMITER_CHAR) for
                 item in test_txt.split("\n")]
        items.pop()

        self.assertListEqual(items, results)

    def test_parse_file_w_line_and_few_items(self):
        test_txt = "1234:foo:bar"
        with patch('builtins.open', mock_open(read_data=test_txt)):
            with self.assertRaises(LineItemsCountError):
                parse_file('foo')
