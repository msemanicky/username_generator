"""Generates list of usernames.

Program generates a list of usernames. The input data is stored in one or
more text files with encoding UTF-8. Each line in the input file is a
record about one user and includes: ID, forename, middlename (optional),
surname and department. Items in the line are separated by colons ':'.
There is only one output file with all records together. Each line in the
output file is a record about one user and includes: ID, generated username,
forename, middle name (optional), surname and department.
"""
import argparse
import sys
import unicodedata


FILE_ENCODING = "utf-8"
USER_ATTRS = "user_id first_name mid_name last_name deppartment".split()

DELIMITER_CHAR = ":"
MIN_ITEMS_COUNT = 4
MAX_ITEMS_COUNT = 5
OUTPUT_DELIMITER_CHAR = ":"


class Error(Exception):
    """Base error class.

    Attributes:
        expression -- input expression in which the error occurred
        message -- explanation of the error
    """

    def __init__(self, expression=None, message=None):
        """Init class."""
        if expression:
            self.expression = expression
        if message:
            self.message = message


class EmptyFileError(Error):
    """Raise when parsing empty file."""

    message = "File is empty."


class DelimiterError(Error):
    """Raise when line does not contain any delimiter char."""

    message = "Line does not contain delimiter. Use {} as delimiter between \
items.".format(DELIMITER_CHAR)


class LineItemsCountError(Error):
    """Raise when count of items does not fit."""

    message = "Line does not contain required count of items. Use line with \
{} or {} items delimited by '{}'.".format(
        MIN_ITEMS_COUNT, MAX_ITEMS_COUNT, DELIMITER_CHAR)


class DuplicityError(Error):
    """Exception raised when username conflicts with another username."""

    message = "Username already used by another user."


class EmptyUsernameError(Error):
    """Exception raised when adding user to users list and username not set."""

    message = "Username must be set."


class NoUserIdError(Error):
    """Exception raised when user id is not set."""

    message = "User id must be set and must be integer number."


class UserIdIntegerError(Error):
    """Exception raised when user id is not integer number."""

    message = "User id must be integer number."


class PositionalAttributesError(Error):
    """Exception raised when try to instantiate class with positional args."""

    message = "Positional attributes are not allowed."


class KeywordAttributesError(Error):
    """Exception raised when try to instantiate class with not defined att."""

    message = "Not defined attribute can not be used."


class NotValidValuesError(Error):
    """Exception raised when object initialized with not correct values."""

    message = "Not valid values specified."


class AsciiConversionError(Error):
    """Exception raised when conversion to ascii failed."""

    message = "Can not convert all characters to ASCII when generating \
username."


class User(object):
    """User object class."""

    # max username lengh withou index number
    USERNAME_BASE_MAX_LENGTH = 8

    def base_value_check(self, value):
        """Base check of value.

        Return True if value is set and contains only alpha numeric chars.
        """
        if not value:
            return False
        if not value.isalnum():
            return False
        return True

    def _remove_accents(self, input_str):
        """Return string without accents."""
        ascii_string = ""
        try:
            nfkd_form = unicodedata.normalize('NFKD', input_str)
            ascii_string = str(nfkd_form.encode('ASCII', 'ignore'), 'ascii')
        except:
            raise AsciiConversionError(input_str)

        return ascii_string

    def create_username(self, indexed=False, index=1):
        """Generate user username.

        Set user username as combination of:
            - first char of user first_name
            - first char of user middle name (optional)
            - user surname

        Max length of username is set by USERNAME_BASE_MAX_LENGTH.

        If indexed, then index is added at the end of username.
        """
        for value in [self.first_name, self.last_name]:
            if not self.base_value_check(value):
                raise NotValidValuesError(value)

        username = self._remove_accents(self.first_name[0])

        if self.mid_name:
            if not self.base_value_check(self.mid_name):
                raise NotValidValuesError(self.mid_name)
            username = "{}{}".format(
                username,
                self._remove_accents(self.mid_name[0]))

        username = "{}{}".format(username,
                                 self._remove_accents(self.last_name))

        self.username = username[:self.USERNAME_BASE_MAX_LENGTH]

        if indexed:
            self.username = "{}{}".format(self.username, index)

        self.username = self.username.lower()

    def get_values_by_attrs(self, user_attrs):
        """Return user object values in order defined by user_attrs."""
        return [getattr(self, attr) for attr in user_attrs]

    def __init__(self, *args, **kwargs):
        """User object init."""
        self.user_id = ""
        self.first_name = ""
        self.mid_name = ""
        self.last_name = ""
        self.username = ""
        self.deppartment = ""
        if args:
            raise PositionalAttributesError(args)

        for key, value in kwargs.items():
            if hasattr(self, key):
                setattr(self, key, value)
            else:
                raise KeywordAttributesError(key)


class Users(object):
    """Users class.

    Users object contains list of user objects.
    """

    @property
    def users(self):
        """Return list of users."""
        return self.users_list

    def is_username_used(self, username):
        """Return True if userslist contains user with specified username."""
        if username in self.usernames:
            return True
        return False

    def check_user_id(self, user_id):
        """Check user ID value."""
        if not user_id:
            raise NoUserIdError()

        try:
            int(user_id)
        except:
            raise UserIdIntegerError(user_id)

    def add(self, user_obj):
        """Add user to userslist."""
        if not user_obj.username:
            raise EmptyUsernameError()

        self.check_user_id(user_obj.user_id)

        if not self.is_username_used(user_obj.username):
            self.users_list.append(user_obj)
            self.usernames.append(user_obj.username)
        else:
            raise DuplicityError(user_obj.username)

    def __iter__(self):
        """Iterator over users objects."""
        for user in self.users_list:
            yield user

    def __len__(self):
        """Return count of users in userslist."""
        return len(self.users_list)

    def __init__(self):
        """Object init."""
        self.usernames = list()
        self.users_list = list()


# File parser section

def check_lines(lines):
    """Check for empty lines.

    Raise EmptyFileError if lines is not instance of type list or
    lines does not contain line.

    Arguments:
        lines -- list of lines to be parsed
    """
    if not lines:
        raise EmptyFileError(lines)

    if not isinstance(lines, list):
        raise EmptyFileError(lines)


def check_line(line):
    """Check parsed line for errors.

    Arguments:
        line -- list of delimited line items
    """
    if not isinstance(line, list):
        raise DelimiterError(line)

    # check for right delimited items count
    if len(line) < MIN_ITEMS_COUNT or len(line) > MAX_ITEMS_COUNT:
        raise LineItemsCountError(line)


def parse_file(filename, encoding="utf-8"):
    """Open file and return list of parsed lines.

    Returns list of lines, where each line containf list of its items.

    Arguments:
        filename -- file name
        encoding -- used file encoding. Default is utf-8.
    """
    parsed_lines = list()

    with open(filename, 'r', encoding=encoding) as file_desc:
        lines = file_desc.readlines()

    check_lines(lines)

    for line in lines:
        parsed_line = parse_line(line)
        check_line(parsed_line)
        parsed_lines.append(parsed_line)

    return parsed_lines


def parse_line(line):
    """Return list of parsed items.

    If line contain DELIMITER_CHAR return list of items delimited by
    DELIMITER_CHAR else return empty string.

    Arguments:
        line -- line to parse
    """
    if line.find(DELIMITER_CHAR) != -1:
        return line.strip().split(":")
    return ""


# Command line arguments section."""

def add_arguments(parser):
    """Set available command line arguments.

    Arguments:
        parser -- ArgumentParser object
    """
    parser.add_argument(
        'input_file',
        nargs='+',
        metavar="INPUT_FILE",
        help="an input file"
    )
    parser.add_argument(
        '-o', '--output',
        required=True,
        metavar="OUTPUT_FILE",
        help="an output file"
    )

    return parser


def get_argument_parser():
    """Return a new ArgumentParser object."""
    return argparse.ArgumentParser()


def get_args():
    """Return command line arguments."""
    parser = add_arguments(get_argument_parser())
    args = parser.parse_args()
    return args


# Process section.

def create_username(user, userslist):
    """Generate unique username."""
    user.create_username()
    index = 0
    while userslist.is_username_used(user.username):
        index += 1
        user.create_username(indexed=True, index=index)


def populate_userslist(users, users_informations=[]):
    """Create users and adds them insto users list.

    Arguments:
        users -- Users object
        users_informations -- list of user informations to create new users
    """
    for user_informations in users_informations:
        user_initial = dict(zip(USER_ATTRS, user_informations))
        user = User(**user_initial)
        create_username(user, users)
        users.add(user)

    return users


def dump_user(user):
    """Return line with user delimited parameters."""
    user_attrs = "{} username ".format(USER_ATTRS[0]).split()
    user_attrs.extend(USER_ATTRS[1:])

    return OUTPUT_DELIMITER_CHAR.join(user.get_values_by_attrs(user_attrs))


def dump_users(users, fd=None):
    """Dump users from userslist.

    Resturn line with all users.

    Writes users into file if fd is set.
    """
    dump = ""
    if users:
        for user in users:
            if fd:
                fd.write("{}\n".format(dump_user(user)))
            else:
                dump = "{}{}\n".format(dump, dump_user(user))
    return dump


def show_errors(user_msg, e):
    """Print errors."""
    print(user_msg)
    print("'{}'".format(e))
    try:
        print(e.message)
    except:
        pass
    print(type(e))


def main(args):
    """Main."""
    # open file for output
    try:
        file_desc = open(args.output, 'w', encoding=FILE_ENCODING)
    except Exception as e:
        msg = "Error occurred whil opening output file."
        show_errors(msg, e)
        sys.exit(2)

    # create new users list
    users = Users()

    # read all input files
    users_informations = list()
    for input_file in args.input_file:
        try:
            users_informations.extend(
                parse_file(input_file, encoding=FILE_ENCODING))

        except EmptyFileError:
            pass

        except Exception as e:
            msg = "Error occurred while parsing file: {}".format(input_file)
            show_errors(msg, e)
            sys.exit(2)

    # populating users list with users
    try:
        populate_userslist(users, users_informations)
    except Exception as e:
        msg = "Error occurred while processing values."
        show_errors(msg, e)
        sys.exit(2)

    # dumping users to file
    dump_users(users, file_desc)

    file_desc.close()


if __name__ == '__main__':
    """Main."""
    args = get_args()
    main(args)
    sys.exit(1)
