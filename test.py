"""Module tests."""
import difflib
import json
import sys
from pprint import pprint


FILE_ENCODING = "utf-8"
ITEMS_DELIMITER = ":"


def test_step(module, test):
    """Test step."""
    users = module.Users()

    raw_data = test['input'].split("\n")

    module.check_lines(raw_data)

    parsed_lines = list()
    users_informations = list()

    for line in raw_data:
        parsed_line = module.parse_line(line)
        module.check_line(parsed_line)
        parsed_lines.append(parsed_line)

    users_informations.extend(parsed_lines)
    module.populate_userslist(users, users_informations)
    result = module.dump_users(users)

    del(users)
    return result


def run_tests(module, tests):
    """Run tests."""
    def show_exception_raied(exception):
        print("Exception raised. {}".format(exception))
        print(type(exception))

    def accept_exception(test, results):
        if 'exception' in results:
            if 'exception' in test and test['exception']:
                module_exc = module.__dict__[test['exception']]
                if type(results['exception']) == module_exc:
                    return True
                else:
                    print("Expected error: {}".format(test['exception']))
                    show_exception_raied(results['exception'])
            else:
                show_exception_raied(results['exception'])

        return False

    def handle_results(test, results):
        if accept_exception(test, results):
            return True
        if 'output' in results and 'output' in test:
            if results['output'] == test['output']:
                return True
        return False

    test_index = 0
    test_success = 0
    test_failed = 0

    # run tests
    for test in tests:
        test_index += 1
        sys.stdout.write("Test {}:".format(test_index))
        results = {}
        try:
            # run test
            results['output'] = test_step(module, test)
        except Exception as e:
            results['exception'] = e

        if not handle_results(test, results):
            print("Failed")

            d = difflib.Differ()
            output = results['output'] if 'output' in results else ""
            expect = test['output'] if 'output' in test else ""
            r = list(d.compare([output, ], [expect, ]))
            print("Output:{}".format(output.strip()))
            print("Expected:{}".format(expect.strip()))
            print("Differences:")
            pprint(r)
            test_failed += 1
        else:
            print("Success")
            test_success += 1

        print("{}".format('-' * 80))

    print("Run {} tests. OK={} Failed={}\n".format(
        test_index, test_success, test_failed))


def main(args):
    """Main."""
    module_file = args[1]
    test_file = args[2]

    module_name = module_file.replace(".py", "")

    module = None

    try:
        from importlib.machinery import SourceFileLoader
        module = SourceFileLoader(module_file, module_file).load_module()
    except Exception as e:
        print("Can not import module")
        print(e)

    try:
        import importlib.util
        spec = importlib.util.spec_from_file_location(module_name, module_file)
        module = importlib.util.module_from_spec(spec)
        spec.loader.exec_module(module)
    except Exception as e:
        print("Can not import module")
        print(e)

    if not module:
        print("Module not loaded.")
        sys.exit(2)

    try:
        with open(test_file, 'r', encoding=FILE_ENCODING) as fd:
            json_data = json.load(fd)
    except Exception as e:
        print("Can not read test data file")
        print(e)
        sys.exit(2)

    run_tests(module, json_data)

if __name__ == '__main__':
    main(sys.argv)
